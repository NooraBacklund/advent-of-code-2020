using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var adapterJoltages = File.ReadAllLines("day10_input.txt").Select(int.Parse).ToList();

            //Add in device and charging outlet
            adapterJoltages.Add(0);
            adapterJoltages.Add(adapterJoltages.Max() + 3);

            //Part 1
            //Order list from smallest joltage to biggest
            adapterJoltages = adapterJoltages.Select(j => j).OrderBy(j => j).ToList();

            //Count 1-jolt and 3-jolt differences
            var jolts1 = 0;
            var jolts3 = 0;

            for (int i = 0; i < adapterJoltages.Count - 1; i++)
            {
                switch (adapterJoltages[i+1] - adapterJoltages[i])
                {
                    case 1:
                        jolts1++;
                        break;
                    case 3:
                        jolts3++;
                        break;
                    default:
                        break;
                }
            }

            //Report answer
            Console.WriteLine($"Part 1 answer: {jolts1 * jolts3}");

            //Part 2
            //Start working the joltage list backwards to determine number of arrangements
            adapterJoltages = adapterJoltages.Select(j => j).OrderByDescending(j => j).ToList();

            //Set up dictionary with info on how many ways to connect to device exist for each adapter
            //initialize with device info
            var variations = new Dictionary<int, long>
            {
                { adapterJoltages.First(), 1 }
            };

            //Calculate possible ways for all adapters to connect to next adapter (skip the device itself)
            foreach (var joltage in adapterJoltages.Skip(1))
            {
                long waysFromThisJoltage = 0;
                waysFromThisJoltage += variations.TryGetValue(joltage + 1, out var i) ? i : 0;
                waysFromThisJoltage += variations.TryGetValue(joltage + 2, out var j) ? j : 0;
                waysFromThisJoltage += variations.TryGetValue(joltage + 3, out var k) ? k : 0;
                variations.Add(joltage, waysFromThisJoltage);
            }

            //Report answer
            Console.WriteLine($"Part 2 answer: {variations[0]}");
        }
    }
}
