﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day4
{
    class Passport
    {
        public string BirthYear;
        public string IssueYear;
        public string ExpirationYear;
        public string Height;
        public string HairColor;
        public string EyeColor;
        public string PassportID;
        public string CountryID;
        public bool MandatoryValuesExist;
        public bool AllValuesValid;

        public Passport(string data)
        {
            //Parse data 
            var details = DataToKeyValuePairs(data);
            //Set values
            MandatoryValuesExist = details.TryGetValue("byr", out BirthYear)
                      && details.TryGetValue("iyr", out IssueYear)
                      && details.TryGetValue("eyr", out ExpirationYear)
                      && details.TryGetValue("hgt", out Height)
                      && details.TryGetValue("hcl", out HairColor)
                      && details.TryGetValue("ecl", out EyeColor)
                      && details.TryGetValue("pid", out PassportID)
                      && true;
            //Except this one isn't mandatory, just try to set it
            details.TryGetValue("cid", out CountryID);

            //Set second step of validation
            AllValuesValid = ValidationPart2();
        }

        private Dictionary<string, string> DataToKeyValuePairs(string data)
        {
            var ret = new Dictionary<string, string>();
            var entries = data.Split(' ');
            foreach (var entry in entries)
            {
                ret[entry.Split(':').First()] = entry.Split(':').Last();
            }
            return ret;
        }

        private bool ValidationPart2()
        {
            var eyeColors = new List<string>() { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
            return int.Parse(BirthYear ?? "0") >= 1920 && int.Parse(BirthYear ?? "9999") <= 2002
                   && int.Parse(IssueYear ?? "0") >= 2010 && int.Parse(IssueYear ?? "9999") <= 2020
                   && int.Parse(ExpirationYear ?? "0") >= 2020 && int.Parse(ExpirationYear ?? "9999") <= 2030
                   && Regex.Match(Height ?? "empty", @"(^1([5-8][0-9]|9[0-3])cm)|((^59|^6[0-9]|^7[0-6])in)$").Success
                   && Regex.Match(HairColor ?? "empty", @"^#([0-9a-f]){6}$").Success
                   && eyeColors.Contains(EyeColor ?? "empty")
                   && Regex.Match(PassportID ?? "empty", @"^[0-9]{9}$").Success
                   && true;
        }
    }
}
