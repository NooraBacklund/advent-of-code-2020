using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in file data, split to individual passport datasets
            var passportData = File.ReadAllText("day4_input.txt").Replace("\r\n\r\n", "|").Replace("\r\n", " ").Split('|');
            var passportList = new List<Passport>();

            // Initialize passports
            foreach (var line in passportData)
            {
                passportList.Add(new Passport(line));
            }

            // Count valid passports
            Console.WriteLine($"Passports with mandatory data (part 1): {passportList.Count(p => p.MandatoryValuesExist)}");
            Console.WriteLine($"Passports with valid data (part 2): {passportList.Count(p => p.AllValuesValid)}");
        }
    }
}
