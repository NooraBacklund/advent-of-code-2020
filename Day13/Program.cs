using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var data = File.ReadAllLines("day13_input.txt");

            //Extract relevant data
            int.TryParse(data.First(), out var departTime);
            var busIDs = data.Last()
                .Split(',')
                .Select(r => r)
                .Where(r => Regex.Match(r, @"\d+").Success)
                .Select(int.Parse)
                .ToList();

            //Part 1
            //Report result
            Console.WriteLine("Part 1 solution - bus with least wait time:\n");
            Console.WriteLine($"Departure time: {departTime}\n");
            busIDs.ForEach(r => Console.WriteLine($"Wait time for bus {r}:\t{r - departTime % r} minutes." +
                $"\tMultiplier: {r * (r - departTime % r)}"));
            Console.WriteLine();

            //Part 2
            //Get time offsets for each required departure
            var part2List = data.Last()
                .Split(',')
                .ToList();
            var offsets = new List<int>();
            var val = new Dictionary<long, long>();

            for (int i = 0; i < part2List.Count; i++)
            {
                if (Regex.Match(part2List[i], @"\d+").Success)
                {
                    offsets.Add(i);
                    val.Add(long.Parse(part2List[i]), i);
                }
            }

            long jump = val.OrderByDescending(x => x.Key).First().Key;
            long time = val.OrderByDescending(x => x.Key).First().Value - val.OrderByDescending(x => x.Key).First().Key + jump;

            for (var i = 1; i <= val.Count; i++)
            {
                while(val.Take(i).Any(t => (time + t.Value) % t.Key != 0))
                {
                    time += jump;
                }
                jump = val.Take(i).Select(t => t.Key).Aggregate((x, y) => LCM(x, y));
            }
            Console.WriteLine($"Part 2 answer: {time}\n");
        }

        public static long GCD(long a, long b)
        {
            while (b != 0)
            {
                long temp = b;
                b = a % b;
                a = temp;
            }

            return a;
        }

        public static long LCM(long a, long b)
        {
            return (a / GCD(a, b)) * b;
        }
    }
}
