﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13
{
    public static class ChineseRemainderTheorem
    {
        public static long Solve(List<int> n, List<int> a)
        {
            var prod = n.Aggregate(1, (i, j) => i * j);
            int p;
            var sum = 0;
            for (var i = 0; i < n.Count; i++)
            {
                p = prod / n[i];
                sum += a[i] * ModularMultiplicativeInverse(p, n[i]) * p;
            }
            return sum % prod;
        }

        public static int ModularMultiplicativeInverse(int a, int mod)
        {
            var b = a % mod;
            for (var i = 0; i < mod; i++)
            {
                if((b * i) % mod == 1)
                {
                    return i;
                }
            }
            return 1;
        }
    }
}
