﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    class TreeLayer
    {
        private readonly List<bool> containsTree;

        public TreeLayer(string treeData)
        {
            containsTree = new List<bool>();
            ParseTreeData(treeData);
        }

        private void ParseTreeData(string treeData)
        {
            foreach (var location in treeData)
            {
                containsTree.Add(location == '#');
            }
        }

        public bool LocationHasTree(int location)
        {
            return containsTree[location % containsTree.Count];
        }
    }
}
