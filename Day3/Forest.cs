﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    class Forest
    {
        public List<TreeLayer> TreeArea;

        public Forest(string[] forestLayout)
        {
            TreeArea = new List<TreeLayer>();
            BuildForest(forestLayout);
        }

        private void BuildForest(string[] forestLayout)
        {
            foreach (var line in forestLayout)
            {
                TreeArea.Add(new TreeLayer(line));
            }
        }
    }
}
