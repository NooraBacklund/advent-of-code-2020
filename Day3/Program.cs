using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var forestData = File.ReadAllLines("day3_input.txt");

            //Build forest
            var forest = new Forest(forestData);

            //Calculate tree collisions
            List<long> collisionCounts = new List<long>
            {
                NumberOfTreesHit(forest, 1, 1),
                NumberOfTreesHit(forest, 3, 1), // Part 1 solution in this row
                NumberOfTreesHit(forest, 5, 1),
                NumberOfTreesHit(forest, 7, 1),
                NumberOfTreesHit(forest, 1, 2)
            };

            Console.WriteLine("Number of trees hit in each run: ");
            foreach (var run in collisionCounts)
            {
                Console.WriteLine(run);
            }

            Console.WriteLine("\nAll collisions multiplied:");
            Console.WriteLine(collisionCounts.Aggregate((x, y) => x * y));
            Console.WriteLine();
        }

        private static int NumberOfTreesHit(Forest forest, int right, int down)
        {
            var location = 0;
            var layer = 0;
            var numTreesHit = 0;

            foreach (var slopeLayer in forest.TreeArea)
            {
                if (layer % down == 0)
                {
                    if (slopeLayer.LocationHasTree(location))
                    {
                        numTreesHit++;
                    }
                    location += right;
                }
                layer++;
            }

            return numTreesHit;
        }
    }
}
