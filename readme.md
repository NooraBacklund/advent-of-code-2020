# Advent of Code 2020

This repository contains source code for Advent of Code 2020 challenge. 
The goal for this year was to get more practice in with C#, especially
with object-oriented method of solving the challenges. Focus has also
been placed on using LINQ when possible and Regex where applicable.