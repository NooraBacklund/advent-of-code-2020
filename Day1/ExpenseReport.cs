﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class ExpenseReport
    {
        private List<int> expenseList;

        public List<int> ExpenseList
        {
            get { return expenseList; }
        }

        public void PopulateExpenseList(string filename)
        {
            try
            {
                var data = File.ReadLines(filename);
                expenseList = data.Select(i => int.Parse(i)).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine("Encountered error while opening file: " + e.Message);
                throw;
            }
        }

        public void PrintExpenses()
        {
            foreach (var item in expenseList)
            {
                Console.WriteLine(item);
            }
        }
    }
}
