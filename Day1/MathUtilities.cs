﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class MathUtilities
    {
        public static IEnumerable<int> GetSumFactors(int sum, IEnumerable<int> numberList, int numberCount)
        {
            //Check if list contains enough values for sum to be found
            if (numberCount > numberList.Count())
            {
                return null;
            }

            //Use a copy of the provided list (not original list)
            var numberPool = numberList.ToList();

            //initialize return value list
            var numbers = new List<int>();

            //Find sum of two items (base case)
            if (numberCount == 2)
            {
                while (numberPool.Count >= numberCount)
                {
                    //Pop first number off the list, only work with the rest
                    var a = numberPool[0];
                    numberPool.RemoveAt(0);

                    //Check if a pair of numbers adding up to given sum exists in list
                    //If so, these are the wanted numbers, return them.
                    if (numberPool.Contains(sum - a))
                    {
                        numbers.Add(a);
                        numbers.Add(sum - a);
                        return numbers;
                    }
                }
            }
            else //recurse until we get base case
            {
                while (numberPool.Count >= numberCount)
                {
                    //Get first number
                    var a = numberPool[0];
                    numberPool.RemoveAt(0);

                    //Find the numbers for the remaining sum
                    var recursedNumbers = MathUtilities.GetSumFactors(sum - a, numberPool, numberCount - 1);
                    if (recursedNumbers != null)
                    {
                        //Add the numbers to results to be returned
                        numbers.Add(a);
                        foreach (var num in recursedNumbers)
                        {
                            numbers.Add(num);
                        }
                        return numbers;
                    }
                }
            }
            //Return null if no results were found in data
            return null;
        }

        public static int MultiplyNumbers(IEnumerable<int> numbers)
        {
            return numbers.Aggregate(1, (x, y) => x * y);
        }
    }
}
