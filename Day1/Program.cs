using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reading in data...");
            var expenseReport = new ExpenseReport();
            expenseReport.PopulateExpenseList("day1_input.txt");

            Console.WriteLine("Locating numbers for sum...");
            var numbers = MathUtilities.GetSumFactors(2020, expenseReport.ExpenseList, 3);

            //Check if results found, print to console if found
            if (numbers == null)
            {
                Console.WriteLine("No suitable numbers found.");
            }
            else
            {
                Console.WriteLine("\nResults: ");
                foreach (var number in numbers)
                {
                    Console.WriteLine(number);
                }
                Console.WriteLine("\nMultiplication result: ");
                Console.WriteLine(MathUtilities.MultiplyNumbers(numbers) + "\n");
            }

        }
    }
}
