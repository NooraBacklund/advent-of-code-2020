using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day7
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Read input
                var data = File.ReadAllLines("day7_input.txt");

                //Process bag rules
                var ruleList = new List<BagRule>();

                foreach (var rule in data)
                {
                    //Console.WriteLine(rule);
                    ruleList.Add(new BagRule(rule));
                }

                //Search for "shiny gold" bags (Part 1 answer)
                var results = GetBagsContainingColor("shiny gold", ruleList);
                Console.WriteLine($"Part 1: number of bags containing shiny gold bags: {results.Distinct().Count()}");

                //Part 2: number of bags inside shiny gold bag
                Console.WriteLine($"Part 2: number of bags inside a shiny gold bag: {GetNumContainedBags("shiny gold", ruleList)}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        //Part 2
        private static int GetNumContainedBags(string color, List<BagRule> ruleList)
        {
            int numBags = 0;

            //Bags contained in the first given bag
            var targetBag = ruleList.Where(r => r.Color.Equals(color)).First();
            numBags = targetBag.NumContainedBags;

            //Calculate number of bags within inner bags
            foreach (var c in targetBag.ContainedColors)
            {
                numBags += GetNumContainedBags(c.Key, ruleList) * c.Value;
            }

            return numBags;
        }

        //Part 1
        private static List<BagRule> GetBagsContainingColor(string color, List<BagRule> ruleList)
        {
            var results = ruleList.Where(r => r.ContainsColor(color)).ToList();
            foreach (var entry in ruleList.Where(r => r.ContainsColor(color)).ToList())
            {
                results.AddRange(GetBagsContainingColor(entry.Color, ruleList));
            }
            return results.ToList();
        }
    }
}
