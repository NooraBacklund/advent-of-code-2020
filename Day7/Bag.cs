﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day7
{
    class BagRule
    {
        public string Color { get; set; }
        public Dictionary<string, int> ContainedColors { get; } = new Dictionary<string, int>();
        public int NumContainedBags { get
            {
                return ContainedColors.Aggregate(0, (x, y) => x + y.Value);
            } }

        public BagRule(string data)
        {
            ParseStringIntoBag(data);
        }

        public void ParseStringIntoBag(string data)
        {
            //set color which the rule concerns
            var rule = data.Split(new string[] { " bags contain " }, StringSplitOptions.RemoveEmptyEntries);
            Color = rule.First();

            //Check if other bags are contained, set colors if so
            if (!rule.Last().Contains("no other"))
            {
                //Set target colors
                rule = rule
                    .Last()
                    .Replace('.', ',')
                    .Replace("bags", "bag")
                    .Split(new string[] { " bag," }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in rule)
                {
                    ContainedColors.Add(
                        Regex.Match(item, @"[a-z]+[a-z ]+").Value,
                        int.Parse(Regex.Match(item, @"\d+").Value)
                        );
                }
            }
        }

        public bool ContainsColor(string color)
        {
            return ContainedColors.TryGetValue(color, out var num);
        }
    }
}
