using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var data = File.ReadAllLines("day11_input.txt").ToList();

            //Add in some floor around the seats
            var floor = new List<string>
            {
                new string('.', data.First().Length + 4),
                new string('.', data.First().Length + 4)
            };
            floor.AddRange(data.Select(r => ".." + r + ".."));
            floor.Add(new string('.', data.First().Length + 4));
            floor.Add(new string('.', data.First().Length + 4));

            //Get stable layout, switch false --> true to display layouts
            var finalLayout = GetNewLayout(floor, false, 1);

            //Part 1 answer: number of seats occupied
            Console.WriteLine($"Part 1: number of occupied seats: " +
                $"{finalLayout.Sum(r => r.Select(c => c).Where(c => c == '#').Count())}");

            //Part 2 layout
            var finalPart2Layout = GetNewLayout(floor, false, 2);
            Console.WriteLine($"Part 2: number of occupied seats: " +
                $"{finalPart2Layout.Sum(r => r.Select(c => c).Where(c => c == '#').Count())}");
        }

        public static int GetNeighborCount(List<string> floor, int row, int col)
        {
            var num = 0;
            for (int i = row - 1; i < row + 2; i++)
            {
                for (int j = col - 1; j < col + 2; j++)
                {
                    if (i != row || j != col)
                    {
                        num += floor[i][j] == '#' ? 1 : 0;
                    }
                }
            }
            return num;
        }

        public static int GetNeighborCountPt2(List <string> floor, int row, int col)
        {
            var num = 0;

            //Check above and below
            var i = row - 1;
            while (i >= 0)
            {
                if (floor[i][col] == 'L') break;
                if (floor[i][col] == '#') { num++; break; }
                i--;
            }

            i = row + 1;
            while (i < floor.Count())
            {
                if (floor[i][col] == 'L') break;
                if (floor[i][col] == '#') { num++; break; }
                i++;
            }

            //Check sides
            var j = col - 1;
            while (j >= 0)
            {
                if (floor[row][j] == 'L') break;
                if (floor[row][j] == '#') { num++; break; }
                j--;
            }

            j = col + 1;
            while (j < floor.First().Length)
            {
                if (floor[row][j] == 'L') break;
                if (floor[row][j] == '#') { num++; break; }
                j++;
            }

            //Check diagonals - left up
            i = row - 1;
            j = col - 1;
            while (i >= 0 && j >= 0)
            {
                if (floor[i][j] == 'L') break;
                if (floor[i][j] == '#') { num++; break; }
                i--;
                j--;
            }

            //left down
            i = row + 1;
            j = col - 1;
            while (i < floor.Count() && j >= 0)
            {
                if (floor[i][j] == 'L') break;
                if (floor[i][j] == '#') { num++; break; }
                i++;
                j--;
            }

            //right up
            i = row - 1;
            j = col + 1;
            while (i >= 0 && j < floor.First().Length)
            {
                if (floor[i][j] == 'L') break;
                if (floor[i][j] == '#') { num++; break; }
                i--;
                j++;
            }

            //right down
            i = row + 1;
            j = col + 1;
            while (i < floor.Count() && j < floor.First().Length)
            {
                if (floor[i][j] == 'L') break;
                if (floor[i][j] == '#') { num++; break; }
                i++;
                j++;
            }

            return num;
        }

        public static List<string> GetNewLayout(List<string> floor, bool printLayout, int part)
        {
            var currFloor = floor.ToList();
            if (printLayout) currFloor.ForEach(r => Console.WriteLine(r));

            //Set up new floor plan
            var rowLength = currFloor.First().Length;
            var newFloor = new List<string>
            {
                new string('.', rowLength),
                new string('.', rowLength)
            };

            //Iterate over seats to get new layout according to rules
            var hasChanges = false;
            for (int i = 2; i < currFloor.Count - 2; i++)
            {
                var str = "";
                for (int j = 2; j < currFloor[i].Length - 2; j++)
                {
                    if (currFloor[i][j] == '.')
                    {
                        str += '.';
                    }
                    else
                    {
                        var chr = 'x';
                        var n = -1;
                        var maxNeighbors = 3;

                        //Get neighbor count
                        if (part == 1)
                        {
                            n = GetNeighborCount(currFloor, i, j);
                        }
                        else if (part == 2)
                        {
                            n = GetNeighborCountPt2(currFloor, i, j);
                            maxNeighbors = 4;
                        }
                        else
                        {
                            throw new NotImplementedException();
                        }
                        
                        // Determine new seat occupancy status
                        if (n == 0)
                        {
                            chr = '#';
                        }
                        else if (n > maxNeighbors)
                        {
                            chr = 'L';
                        }
                        else
                        {
                            chr = currFloor[i][j];
                        }

                        //Determine if layout changed
                        hasChanges = currFloor[i][j] != chr ? true : hasChanges;
                        str += chr;
                    }
                }
                newFloor.Add(".." + str + "..");
            }
            newFloor.Add(new string('.', rowLength));
            newFloor.Add(new string('.', rowLength));

            //If layout was not stable, recurse for next iteration. Otherwise return stable layout.
            if (hasChanges)
            {
                return GetNewLayout(newFloor, printLayout, part);
            } else
            {
                if (printLayout) newFloor.ForEach(r => Console.WriteLine(r));
                return newFloor;
            }
        }
    }
}
