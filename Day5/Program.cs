using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day5
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Read in data
                var rawData = File.ReadAllLines("day5_input.txt");

                //Create listing of seats
                var seats = new List<Seat>();
                foreach (var seat in rawData)
                {
                    seats.Add(new Seat(seat));
                }

                //Part 1: get seat with highest ID
                var maxSeatId = seats.Max(s => s.ID);
                Console.WriteLine($"Part 1: \nHighest seat number: {maxSeatId}\n");

                //Part 2: find missing ID
                //Also determine smallest seat number
                var minSeatId = seats.Min(s => s.ID);

                //Determine missing seats
                var missingSeats = Enumerable.Range(minSeatId, maxSeatId - minSeatId + 1)
                                        .Except(seats.Select(s => s.ID));
                
                //Show results
                Console.WriteLine("Part 2:");
                Console.WriteLine($"Found {missingSeats.Count()} missing seat(s), first missing seat: {missingSeats.First()}\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
