﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    class Seat
    {
        public int Row;
        public int Column;
        public int ID;

        public Seat(string data)
        {
            SetRowAndColumnFromData(data);
            ID = Row * 8 + Column;
        }

        public void SetRowAndColumnFromData(string data)
        {
            //Determine correct row
            int min = 0;
            int max = 127;

            for (int i = 0; i < data.Length - 3; i++)
            {
                if (data[i] == 'F')
                {
                    max -= (max - min + 1) / 2;
                }
                else
                {
                    min += (max - min + 1) / 2;
                }
            }

            //At this point, min == max, set Row
            Row = min;

            //Determine correct column
            min = 0;
            max = 7;

            for (int i = 7; i < data.Length; i++)
            {
                if (data[i] == 'L')
                {
                    max -= (max - min + 1) / 2;
                }
                else
                {
                    min += (max - min + 1) / 2;
                }
            }

            //At this point, min == max, set Column
            Column = min;
        }
    }
}
