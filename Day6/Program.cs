using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day6
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Read in data
                var answers = File.ReadAllText("day6_input.txt").Replace("\r\n\r\n", "|").Split('|');

                //Process answers
                var answerData = new List<AnswerGroup>();
                foreach (var group in answers)
                {
                    answerData.Add(new AnswerGroup(group));
                }

                //Part 1 solution: Sum of distinct answer counts for all groups
                Console.WriteLine($"Part 1 solution: {answerData.Sum(a => a.UniqueAnswerCount)}\n");

                //Part 2 solution: sum of common answer counts for all groups
                Console.WriteLine($"Part 2 solution: {answerData.Sum(a => a.CommonAnswerCount)}\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
