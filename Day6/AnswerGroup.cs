﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6
{
    class AnswerGroup
    {
        public List<string> AnswerList = new List<string>();
        public int UniqueAnswerCount { get { return AnswerList.SelectMany(a => a).Distinct().Count(); } }
        public int CommonAnswerCount { get
            {
                return AnswerList
                    .Skip(1)
                    .Aggregate(
                        new HashSet<char>(AnswerList.First()),
                        (x, y) => { x.IntersectWith(y); return x; }
                    )
                    .Count();
            }
        }

        public AnswerGroup(string data)
        {
            ParseData(data);
        }

        private void ParseData(string data)
        {
            foreach (var answer in data.Replace("\r\n", "|").Split('|'))
            {
                AnswerList.Add(answer);
            }
        }
    }
}
