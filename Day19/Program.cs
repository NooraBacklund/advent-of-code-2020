using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day19
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var rules = File.ReadAllLines("day19_rules.txt");
            var messages = File.ReadAllLines("day19_messages.txt");

            var part1 = new Part1Solver(rules, messages);
            Console.WriteLine($"Part 1 solution: {part1.Solve()}\n");

            var part2 = new Part2Solver(rules, messages);
            Console.WriteLine($"Part 2 solution: {part2.Solve()}\n");
        }
    }
}
