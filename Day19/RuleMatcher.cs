﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day19
{
    class RuleMatcher
    {
        private SortedDictionary<int, string> rules = new SortedDictionary<int, string>();
        private string regex;

        public RuleMatcher(string[] ruleList, int part)
        {
            foreach (var rule in ruleList)
            {
                rules[int.Parse(Regex.Match(rule, @"\d+").Value)] = rule.Split(':').Last();
            }
            if (part == 2) ModPart2Rules();
            regex = GetRegexString();
        }

        public void ModPart2Rules()
        {
            rules[8] = "((42)+)";
            rules[11] = "42 31 | " +
                "42 42 31 31 | " +
                "42 42 42 31 31 31 | " +
                "42 42 42 42 31 31 31 31 | " +
                "42 42 42 42 42 31 31 31 31 31";
            Console.WriteLine("...crunching numbers, please wait");
        }

        private string GetRegexString()
        {
            var rule0 = rules[0];
            while (Regex.Match(rule0, @"\d+").Success)
            {
                var n = Regex.Match(rule0, @"\d+").Value;
                var i = rule0.IndexOf(n);
                rule0 = rule0.Remove(i, n.Length);
                rule0 = rule0.Insert(i, "(" + rules[int.Parse(n)].Trim() + ")");
            }
            //Create a cleaned up regex string
            return rule0
                .Replace("\"", "")
                .Replace("(a)", "a")
                .Replace("(b)", "b")
                .Replace(" ", "");
        }

        public bool IsValid(string message)
        {
            return Regex.Match(message, $"^{regex}$").Success;
        }
    }
}
