﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19
{
    class Part1Solver
    {
        private RuleMatcher rules;
        private string[] messages;

        public Part1Solver(string[] rules, string[] messages)
        {
            this.rules = new RuleMatcher(rules, 1);
            this.messages = messages;
        }

        internal int Solve()
        {
            var numValid = 0;
            foreach (var message in messages)
            {
                numValid += rules.IsValid(message) ? 1 : 0;
            }
            return numValid;
        }
    }
}
