﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15
{
    class Part2Solver
    {
        private readonly Dictionary<long, long> numbers = new Dictionary<long, long>();

        public Part2Solver(List<long> numbers)
        {
            for (int i = 0; i < numbers.Count; i++)
            {
                this.numbers.Add(numbers.ElementAt(i), i);
            }
        }

        public long Solve(int n)
        {
            //Get nth number on list (note that list already has numbers)
            var previous = numbers.Last().Key;
            numbers.Remove(previous);
            var counter = numbers.Count;
            while (counter < n - 1)
            {
                previous = GetNextNumber(numbers, previous, counter);
                counter++;
            }

            //Return the nth number
            return previous;
        }

        private long GetNextNumber(Dictionary<long, long> numbers, long previous, long counter)
        {
            if (!numbers.TryGetValue(previous, out long index))
            {
                numbers[previous] = counter;
                return 0;
            } else
            {
                var next = counter - index;
                numbers[previous] = counter;
                return next;
            }
        }
    }
}
