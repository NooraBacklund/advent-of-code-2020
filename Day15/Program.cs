using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Input
            var numbers = new List<long>() { 17, 1, 3, 16, 19, 0 };

            //Get Part 1 answer
            var part1 = new Part1Solver(numbers);
            Console.WriteLine($"Part 1 answer: {part1.Solve(2020)}\n");

            //Get Part 2 answer
            var part2 = new Part2Solver(numbers);
            Console.WriteLine($"Part 2 answer: {part2.Solve(30000000)}\n");
        }
    }
}
