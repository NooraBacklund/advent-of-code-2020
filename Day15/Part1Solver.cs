﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15
{
    class Part1Solver
    {
        private readonly List<long> numbers;

        public Part1Solver(List<long> numbers)
        {
            this.numbers = numbers.ToList();
        }

        public long Solve(int n)
        {
            //Get nth number on list (note that list already has numbers)
            var counter = numbers.Count;
            while (counter < n)
            {
                numbers.Add(GetNextNumber(numbers));
                counter++;
                //Console.WriteLine(numbers.Last());
            }

            //Return the nth number
            return numbers.Last();
        }

        private long GetNextNumber(List<long> numbers)
        {
            var last = numbers.Last();
            //Check if last number only had one occurrence
            if (numbers.IndexOf(last) == numbers.Count - 1)
            {
                return 0;
            } else
            {
                numbers.Reverse();
                var numSinceLastOccurrence = numbers.Skip(1).ToList().IndexOf(last) + 1;
                numbers.Reverse();
                return numSinceLastOccurrence;
            }
        }
    }
}
