﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day17
{
    class Part1Solver
    {
        private List<Coordinate> activeCoordinates = new List<Coordinate>();

        public Part1Solver(List<string> data)
        {
            //Fetch active coordinates
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data.ElementAt(i).Length; j++)
                {
                    if (data.ElementAt(i)[j] == '#')
                    {
                        //Only add active coordinates to list
                        activeCoordinates.Add(new Coordinate(i, j, 0));
                    }
                }
            }
        }

        public int Solve(int n)
        {
            for (int i = 0; i < n; i++)
            {
                CycleCoordinates();
            }
            return activeCoordinates.Count;
        }

        private void CycleCoordinates()
        {
            //Initialize new coordinate set
            var newCoordinates = new List<Coordinate>();

            //Get cubespace bounds
            var minX = activeCoordinates.Min(c => c.X);
            var maxX = activeCoordinates.Max(c => c.X);
            var minY = activeCoordinates.Min(c => c.Y);
            var maxY = activeCoordinates.Max(c => c.Y);
            var minZ = activeCoordinates.Min(c => c.Z);
            var maxZ = activeCoordinates.Max(c => c.Z);

            //Get neighbor counts for all coordinates that may have neighbors
            for (int x = minX - 1; x <= maxX + 1; x++)
            {
                for (int y = minY - 1; y <= maxY + 1; y++)
                {
                    for (int z = minZ - 1; z <= maxZ + 1; z++)
                    {
                        var coord = new Coordinate(x, y, z);
                        var n = GetNeighborCount(coord);

                        //Check if coordinate is active
                        if (activeCoordinates.Contains(coord))
                        {
                            if (n == 2 || n == 3) newCoordinates.Add(coord);
                        }
                        else
                        {
                            if (n == 3) newCoordinates.Add(coord);
                        }
                    }
                }
            }

            //Replace active coordinates with new set
            activeCoordinates = newCoordinates;
        }

        private int GetNeighborCount(Coordinate coordinate)
        {
            var n = 0;
            for (int x = coordinate.X - 1; x <= coordinate.X + 1; x++)
            {
                for (int y = coordinate.Y - 1; y <= coordinate.Y + 1; y++)
                {
                    for (int z = coordinate.Z - 1; z <= coordinate.Z + 1; z++)
                    {
                        n += activeCoordinates.Contains(new Coordinate(x, y, z)) ? 1 : 0;
                    }
                }
            }

            //Subtract self if counted as a neighbor
            n -= activeCoordinates.Contains(coordinate) ? 1 : 0;

            return n;
        }
    }
}
