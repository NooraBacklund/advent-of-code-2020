﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day17
{
    class Part2Solver
    {
        private List<Coordinate> activeCoordinates = new List<Coordinate>();

        public Part2Solver(List<string> data)
        {
            //Fetch active coordinates
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data.ElementAt(i).Length; j++)
                {
                    if (data.ElementAt(i)[j] == '#')
                    {
                        //Only add active coordinates to list
                        activeCoordinates.Add(new Coordinate(i, j, 0, 0));
                    }
                }
            }
        }

        public int Solve(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"...solving round {i + 1}");
                CycleCoordinates();
            }
            return activeCoordinates.Count;
        }

        private void CycleCoordinates()
        {
            //Initialize new coordinate set
            var newCoordinates = new List<Coordinate>();

            //Create a list of coordinates that have neighbors
            var possible = new List<Coordinate>();
            foreach (var coord in activeCoordinates)
            {
                for (int x = coord.X - 1; x <= coord.X + 1; x++)
                {
                    for (int y = coord.Y - 1; y <= coord.Y + 1; y++)
                    {
                        for (int z = coord.Z - 1; z <= coord.Z + 1; z++)
                        {
                            for (int w = coord.W - 1; w <= coord.W + 1; w++)
                            {
                                var c = new Coordinate(x, y, z, w);
                                if (!possible.Contains(c))
                                {
                                    possible.Add(c);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var coord in possible)
            {
                //Get neighbor count
                var n = GetNeighborCount(coord);

                //Check if coordinate is active
                if (activeCoordinates.Contains(coord))
                {
                    if (n == 2 || n == 3) newCoordinates.Add(coord);
                }
                else
                {
                    if (n == 3) newCoordinates.Add(coord);
                }
            }

            //Replace active coordinates with new set
            activeCoordinates = newCoordinates;
        }

        private int GetNeighborCount(Coordinate coordinate)
        {
            var n = 0;
            for (int x = coordinate.X - 1; x <= coordinate.X + 1; x++)
            {
                for (int y = coordinate.Y - 1; y <= coordinate.Y + 1; y++)
                {
                    for (int z = coordinate.Z - 1; z <= coordinate.Z + 1; z++)
                    {
                        for (int w = coordinate.W - 1; w <= coordinate.W + 1; w++)
                        { 
                            if (n == 5) return n; //no need to calculate more, this will already make the cell inactive
                            n += activeCoordinates.Contains(new Coordinate(x, y, z, w)) ? 1 : 0;
                        }
                    }
                }
            }
            //Subtract self if counted as a neighbor
            n -= activeCoordinates.Contains(coordinate) ? 1 : 0;
            return n;
        }
    }
}
