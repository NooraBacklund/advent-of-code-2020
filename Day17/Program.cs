using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day17
{
    class Program
    {
        static void Main(string[] args)
        {
            //Set up input data
            var data = new List<string>()
            {
                ".##...#.",
                ".#.###..",
                "..##.#.#",
                "##...#.#",
                "#..#...#",
                "#..###..",
                ".##.####",
                "..#####."
            };


            var sampleData = new List<string>()
            {
                ".#.",
                "..#",
                "###"
            };

            var part1 = new Part1Solver(data);
            Console.WriteLine($"Part 1 answer: {part1.Solve(6)}");

            var part2 = new Part2Solver(data);
            Console.WriteLine($"Part 2 answer: {part2.Solve(6)}");
        }
    }
}
