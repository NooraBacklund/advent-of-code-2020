﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16
{
    class Part2Solver
    {
        private List<int> ticket;
        private List<string> nearby;
        private TicketDefinition fields;

        public Part2Solver(List<int> ticket, string[] nearby, string[] fields)
        {
            this.ticket = ticket;
            this.nearby = nearby.ToList();
            this.fields = new TicketDefinition(fields);
        }

        public long Solve()
        {
            var validTickets = new List<string>();
            foreach (var ticket in nearby)
            {
                //Only fetch valid tickets
                if (fields.IsValidTicket(ticket.Split(',').Select(int.Parse).ToList(), out var value))
                {
                    validTickets.Add(ticket);
                }
            }

            //Determine correct field names
            var matchingFields = fields.GetMatchingFieldNames(fields.GetMatchingRules(validTickets));

            //Find indexes where field name starts with "departure"
            matchingFields = matchingFields.Where(f => f.Key.Contains("departure")).ToDictionary(f => f.Key, f => f.Value);

            //Calculate answer
            long answer = 1;
            matchingFields.Values.ToList().ForEach(i => answer *= ticket.ElementAt(i));

            return answer;
        }
    }
}
