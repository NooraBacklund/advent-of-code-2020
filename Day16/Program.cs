using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var fields = File.ReadAllLines("day16_fields.txt");
            var nearby = File.ReadAllLines("day16_nearby.txt");

            //Own ticket
            var ticket = new List<int>() { 131, 103, 109, 67, 127, 97, 89, 79, 163, 59, 73, 83, 61, 107, 53, 193, 167, 101, 71, 197 };

            var part1 = new Part1Solver(ticket, nearby, fields);
            Console.WriteLine($"Part 1 answer: {part1.Solve()}\n");

            var part2 = new Part2Solver(ticket, nearby, fields);
            Console.WriteLine($"Part 2 answer: {part2.Solve()}\n");
        }
    }
}
