﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16
{
    class Part1Solver
    {
        private List<int> ticket;
        private List<string> nearby;
        private TicketDefinition fields;

        public Part1Solver(List<int> ticket, string[] nearby, string[] fields)
        {
            this.ticket = ticket;
            this.nearby = nearby.ToList();
            this.fields = new TicketDefinition(fields);
        }

        public int Solve()
        {
            var invalidValues = new List<int>();
            foreach (var ticket in nearby)
            {
                if (!fields.IsValidTicket(ticket.Split(',').Select(int.Parse).ToList(), out var value))
                {
                    invalidValues.Add(value);
                }
            }
            return invalidValues.Sum();
        }
    }
}
