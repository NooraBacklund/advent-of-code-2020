﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16
{
    class TicketDefinition
    {
        private List<Rule> ruleList = new List<Rule>();

        public TicketDefinition(string[] ticket)
        {
            ticket.ToList().ForEach(t => ruleList.Add(new Rule(t)));
        }

        private bool ValueValidForTicket(int value)
        {
            var ret = false;
            foreach (var rule in ruleList)
            {
                if (rule.IsValid(value)) ret = true;
            }
            return ret;
        }

        public bool IsValidTicket(List<int> ticket, out int invalidValue)
        {
            invalidValue = -1;
            foreach (var value in ticket)
            {
                invalidValue = value;
                if (!ValueValidForTicket(value)) return false;
            }
            return true;
        }

        public List<List<string>> GetMatchingRules(List<string> validTickets)
        {
            var fieldMatches = new List<List<string>>();
            for (int i = 0; i < ruleList.Count; i++)
            {
                //Get possible field names for each field on ticket
                var matches = new List<string>();
                var fieldData = validTickets.Select(t => t.Split(',').Select(int.Parse).ElementAt(i)).ToList();
                foreach (var rule in ruleList)
                {
                    if (fieldData.All(f => rule.IsValid(f))) matches.Add(rule.Name);
                }
                fieldMatches.Add(matches);
            }
            return fieldMatches;
        }

        public Dictionary<string, int> GetMatchingFieldNames(List<List<string>> fieldMatches)
        {
            var fieldNames = new Dictionary<string, int>();

            while (fieldMatches.Any(l => l.Count > 1))
            {
                var index = -1;
                var name = "";
                //Get index with only one match
                for (int i = 0; i < fieldMatches.Count; i++)
                {
                    if (fieldMatches[i].Count == 1)
                    {
                        index = i;
                        name = fieldMatches[i].First();
                        fieldNames.Add(name, i);
                        break;
                    }
                }

                //Remove entry from all lists
                fieldMatches.ForEach(l => l.Remove(name));
            }
            return fieldNames;
        }
    }
}
