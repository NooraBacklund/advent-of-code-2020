﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day16
{
    class Rule
    {
        public string Name { get; }
        public int Min1 { get; }
        public int Max1 { get; }
        public int Min2 { get; }
        public int Max2 { get; }

        public Rule(string input)
        {
            Name = Regex.Match(input, @"[a-z ]+").Value;
            Min1 = int.Parse(Regex.Match(input, @"\d+").Value);
            Max1 = int.Parse(Regex.Match(input, @"-\d+").Value.Remove(0, 1));
            Min2 = int.Parse(Regex.Match(input, @"or \d+").Value.Remove(0, 3));
            Max2 = int.Parse(Regex.Match(input, @"\d+", RegexOptions.RightToLeft).Value);
        }

        public bool IsValid(int value)
        {
            return (value >= Min1 && value <= Max1) || (value >= Min2 && value <= Max2);
        }
    }
}
