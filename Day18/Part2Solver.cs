﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day18
{
    class Part2Solver
    {
        private string[] data;

        public Part2Solver(string[] data)
        {
            this.data = data;
        }

        public long Solve()
        {
            long sum = 0;
            foreach (var equation in data)
            {
                sum += Evaluate(equation);
            }
            return sum;
        }

        private long Evaluate(string equation)
        {
            var eq = equation;
            while (Regex.Match(eq, @"\([0-9 \+\*]+\)").Success)
            {
                var inner = Regex.Match(eq, @"\([0-9 \+\*]+\)").Value;
                eq = eq.Replace(inner, GetValue(inner).ToString());
            }
            return GetValue(eq);
        }

        private long GetValue(string equation)
        {
            //Remove parentheses around the equation if necessary
            var eq = equation.Replace("(", "");
            eq = eq.Replace(")", "");

            //Parse equation operations and numbers
            var numbers = new List<long>();
            var operations = new List<string>();

            //Extract numbers and store in list
            while (Regex.Match(eq, @"\d+").Success)
            {
                var num = Regex.Match(eq, @"\d+").Value;
                numbers.Add(long.Parse(num));
                eq = eq.Remove(eq.IndexOf(num), num.Length);
            }

            //Extract operations and store in list
            while (Regex.Match(eq, @"[\+\*]").Success)
            {
                var op = Regex.Match(eq, @"[\+\*]").Value;
                operations.Add(op);
                eq = eq.Remove(eq.IndexOf(op), op.Length);
            }

            //Perform additions first
            while (operations.Contains("+"))
            {
                int i = operations.IndexOf("+");
                numbers[i] = numbers[i] + numbers[i + 1];
                numbers.RemoveAt(i + 1);
                operations.RemoveAt(i);
            }

            //Finish off by performing multiplications
            return numbers.Aggregate((x, y) => x * y);
        }
    }
}
