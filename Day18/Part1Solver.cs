﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day18
{
    class Part1Solver
    {
        private string[] data;

        public Part1Solver(string[] data)
        {
            this.data = data;
        }

        public long Solve()
        {
            long sum = 0;
            foreach (var equation in data)
            {
                sum += Evaluate(equation);
            }
            return sum;
        }

        private long Evaluate(string equation)
        {
            var eq = equation;
            while (Regex.Match(eq, @"\([0-9 \+\*]+\)").Success)
            {
                var inner = Regex.Match(eq, @"\([0-9 \+\*]+\)").Value;
                eq = eq.Replace(inner, GetValue(inner).ToString());
            }
            return GetValue(eq);
        }

        private long GetValue(string equation)
        {
            //Remove parentheses around the equation if necessary
            var eq = equation.Replace("(", "");
            eq = eq.Replace(")", "");

            //Parse equation operations and numbers
            var numbers = new Queue<long>();
            var operations = new Queue<char>();

            //Extract first value and enqueue it
            var num = "";
            num = Regex.Match(eq, @"\d+").Value;
            numbers.Enqueue(long.Parse(num));
            eq = eq.Remove(eq.IndexOf(num), num.Length);

            //Continue extracting operations and numbers as long as present
            while (Regex.Match(eq, @"\d+").Success)
            {
                var op = eq[1];
                operations.Enqueue(op);
                num = Regex.Match(eq, @"\d+").Value;
                numbers.Enqueue(long.Parse(num));
                var replace = " " + op.ToString() + " " + num;
                eq = eq.Remove(eq.IndexOf(replace), replace.Length);
            }

            var ret = numbers.Dequeue();
            while (numbers.Count > 0)
            {
                var op = operations.Dequeue();
                if (op == '*')
                {
                    ret *= numbers.Dequeue();
                }
                else if (op == '+')
                {
                    ret += numbers.Dequeue();
                }
            }
            return ret;
        }
    }
}
