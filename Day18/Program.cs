using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read file input
            var data = File.ReadAllLines("day18_input.txt");
            var sampleData = new string[1] { "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" }; // == 13632 (pt1), 23340 (pt2)

            var part1 = new Part1Solver(data);
            Console.WriteLine($"Part 1 solution: {part1.Solve()}\n");

            var part2 = new Part2Solver(data);
            Console.WriteLine($"Part 2 solution: {part2.Solve()}");
        }
    }
}
