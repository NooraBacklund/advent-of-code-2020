using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var numberStream = File.ReadAllLines("day9_input.txt").Select(long.Parse);

            //Part 1
            var invalidNumber = FindInvalidNumber(numberStream);
            Console.WriteLine($"Part 1 solution: {invalidNumber}");

            //Part 2
            var sumFactors = FindSumComponents(invalidNumber, numberStream);
            Console.WriteLine($"Part 2 solution: min: {sumFactors.Min()}, max: {sumFactors.Max()}, sum: {sumFactors.Min() + sumFactors.Max()}");
        }

        public static bool CheckValidity(long num, List<long> preamble)
        {
            //Check if number is valid
            var isValid = IsValidNextNumber(num, preamble);

            //Update preamble
            preamble.RemoveAt(0);
            preamble.Add(num);

            return isValid;
        }

        public static bool IsValidNextNumber(long num, List<long> preamble)
        {
            //End condition for recursion
            if (preamble.Count < 2) { return false; }

            //Initialize variables
            var first = preamble.First();
            var remainingPreamble = preamble.ToList();
            remainingPreamble.RemoveAt(0);

            return remainingPreamble.Contains(num - first) || IsValidNextNumber(num, remainingPreamble);
        }

        public static List<long> FindSumComponents(long num, IEnumerable<long> numberStream)
        {
            var sumComponents = new List<long>() { 0 };
            var numbers = numberStream.ToList();
            var sum = sumComponents.Sum();

            //Continue processing until sum is found or either list runs out of numbers
            //while(num != sumComponents.Sum() && numbers.Count > 0 && sumComponents.Count > 0)
            while (num != sum && numbers.Count > 0 && sumComponents.Count > 0)
            {
                //If sum is below wanted number, add a number to sum, if above, remove first number from sum
                if (sum < num)
                {
                    sumComponents.Add(numbers.First());
                    numbers.RemoveAt(0);
                } else if (sum > num)
                {
                    sumComponents.RemoveAt(0);
                }
                sum = sumComponents.Sum();
            }
           return sumComponents;
        }

        public static long FindInvalidNumber(IEnumerable<long> numbers)
        {
            var numberStream = numbers.ToList();

            //Pop preamble numbers from stream
            var preamble = numberStream.Take(25).ToList();
            numberStream.RemoveRange(0, 25);

            //Determine invalid number
            long num;
            do
            {
                //Pop first number off stream and check validity until invalid number found
                num = numberStream.First();
                numberStream.RemoveAt(0);
            } while (CheckValidity(num, preamble) && numberStream.Count > 1);

            return num;
        }
    }
}
