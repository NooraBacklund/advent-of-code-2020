using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var data = File.ReadAllLines("day12_input.txt").ToList();

            //Part 1
            var ship = new Ship();
            data.ForEach(a => ship.Navigate(a));
            Console.WriteLine($"Part 1 answer: Manhattan distance is {ship.ManhattanDistance}.\n");

            //Part 2
            var ship2 = new Ship();
            data.ForEach(a => ship2.NavigatePart2(a));
            Console.WriteLine($"Part 2 answer: Manhattan distance is {ship2.ManhattanDistance}.\n");
        }
    }
}
