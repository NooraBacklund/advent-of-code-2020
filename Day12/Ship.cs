﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day12
{
    class Ship
    {
        public int X { get; set; } = 0;
        public int Y { get; set; } = 0;
        public int Dir { get; set; } = 1; //N = 0, E = 1, S = 2, W = 3
        public int ManhattanDistance => Math.Abs(X) + Math.Abs(Y);

        //Part 2:
        public int WayX { get; set; } = 10;
        public int WayY { get; set; } = 1;

        private readonly char[] directions = { 'N', 'E', 'S', 'W' };

        public void Navigate(string action)
        {
            //Parse action
            var type = Regex.Match(action, @"[A-Z]").Value;
            var num = int.Parse(Regex.Match(action, @"\d+").Value);

            //Determine correct course of action
            switch (type)
            {
                case "N":
                case "E":
                case "S":
                case "W":
                    Move(type, num);
                    break;
                case "L":
                    Dir = (Dir - (num / 90) + 4) % 4;
                    break;
                case "R":
                    Dir = (Dir + (num / 90)) % 4;
                    break;
                case "F":
                    Move(directions[Dir].ToString(), num);
                    break;
                default:
                    break;
            }
        }

        private void Move(string direction, int distance)
        {
            switch (direction)
            {
                case "N":
                    Y += distance;
                    break;
                case "E":
                    X += distance;
                    break;
                case "S":
                    Y -= distance;
                    break;
                case "W":
                    X -= distance;
                    break;
                default:
                    throw new Exception($"Unknown direction: {direction}");
            }
        }


        public void NavigatePart2(string action)
        {
            //Parse action
            var type = Regex.Match(action, @"[A-Z]").Value;
            var num = int.Parse(Regex.Match(action, @"\d+").Value);

            //Determine correct course of action
            switch (type)
            {
                case "N":
                case "E":
                case "S":
                case "W":
                    MoveWaypoint(type, num);
                    break;
                case "L":
                case "R":
                    RotateWaypoint(type, num);
                    break;
                case "F":
                    MoveShip(num);
                    break;
                default:
                    break;
            }
        }

        private void MoveShip(int distance)
        {
            //Get distance between waypoint and ship
            int x = WayX - X;
            int y = WayY - Y;

            //Move ship
            X += distance * x;
            Y += distance * y;

            //Move waypoint
            WayX = X + x;
            WayY = Y + y;
        }

        private void RotateWaypoint(string direction, int degrees)
        {
            //Determine rotation direction
            var deg = direction == "L" ? degrees : -degrees;

            //Set up variables for rotation
            double rad = (Math.PI / 180) * deg;
            double sin = Math.Sin(rad);
            double cos = Math.Cos(rad);

            //Set waypoint in relation to origin
            WayX -= X;
            WayY -= Y;

            //Rotate
            double x = WayX * cos - WayY * sin;
            double y = WayX * sin + WayY * cos;

            //Restore waypoint in relatio to ship
            WayX = (int) Math.Round(x, 0) + X;
            WayY = (int) Math.Round(y, 0) + Y;
        }

        private void MoveWaypoint(string direction, int distance)
        {
            switch (direction)
            {
                case "N":
                    WayY += distance;
                    break;
                case "E":
                    WayX += distance;
                    break;
                case "S":
                    WayY -= distance;
                    break;
                case "W":
                    WayX -= distance;
                    break;
                default:
                    throw new Exception($"Unknown direction: {direction}");
            }
        }
    }
}
