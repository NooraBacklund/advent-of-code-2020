using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var passwordList = new List<PasswordEntry>();
            InitializeData(passwordList);

            Console.WriteLine("Number of valid passwords according to first set of rules: ");
            Console.WriteLine(passwordList.Count(e => e.IsValidPassword));

            Console.WriteLine("\nNumber of valid passwords according to second set of rules: ");
            Console.WriteLine(passwordList.Count(e => e.IsValidPasswordPart2));
        }

        public static void InitializeData(List<PasswordEntry> pwList)
        {
            try
            {
                var passwordList = File.ReadAllLines("day2_input.txt");
                foreach (var pw in passwordList)
                {
                    pwList.Add(PasswordEntry.ParseLineToPasswordEntry(pw));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
