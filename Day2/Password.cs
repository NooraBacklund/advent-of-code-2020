﻿using System.Linq;

namespace Day2
{
    class PasswordEntry
    {
        private readonly char requiredCharacter;
        private readonly int num1;
        private readonly int num2;

        public string Password;
        public bool IsValidPassword;
        public bool IsValidPasswordPart2;

        public PasswordEntry(string password, char requiredChar, int n1, int n2)
        {
            requiredCharacter = requiredChar;
            num1 = n1;
            num2 = n2;
            Password = password;
            IsValidPassword = CheckValidity();
            IsValidPasswordPart2 = CheckValidityPart2();
        }

        private bool CheckValidity()
        {
            return Password.Count(c => c == requiredCharacter) >= num1
                   && Password.Count(c => c == requiredCharacter) <= num2;
        }

        private bool CheckValidityPart2()
        {
            //Elves don't have index 0, so their "1" is really "0" in programming terms.
            //Check if char at given position is the required char, XOR the results to handle password requirements.
            return (Password.ElementAt(num1 - 1) == requiredCharacter) 
                   ^ (Password.ElementAt(num2 - 1) == requiredCharacter);
        }

        public static PasswordEntry ParseLineToPasswordEntry(string input)
        {
            //Input format example: 3-4 d: ddddasdflkjg
            var password = input.Split(' ').Last();
            var requiredChar = input.Split(' ').ElementAt(1).Split(':').First();
            var num1 = int.Parse(input.Split(' ').First().Split('-').First());
            var num2 = int.Parse(input.Split(' ').First().Split('-').Last());
            return new PasswordEntry(password, requiredChar.First(), num1, num2);
        }
    }
}
