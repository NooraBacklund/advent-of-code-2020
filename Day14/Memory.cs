﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day14
{
    class Memory
    {
        public Dictionary<string, long> AddressList { get; set; } = new Dictionary<string, long>();
        public string Mask { get; set; }

        public void ProcessInstruction(string instruction, int part)
        {
            switch (GetInstructionType(instruction))
            {
                case "mask":
                    Mask = Regex.Match(instruction, @"[10X]+").Value;
                    break;
                case "mem":
                    if (part == 1) SetMemoryValue(instruction);
                    if (part == 2) SetMemoryValue2(instruction);
                    break;
                default:
                    Console.WriteLine($"Unknown instruction: {instruction}");
                    break;
            }
        }

        private void SetMemoryValue2(string instruction)
        {
            //Parse values for memory slot and value to be stored
            var memSlot = Regex.Match(instruction, @"\d+").Value;
            var value = Regex.Match(instruction, @"[0-9]+$").Value;

            //Convert memory address to binary
            memSlot = Convert.ToString(long.Parse(memSlot), 2).PadLeft(Mask.Length, '0');

            //Apply mask to memory address
            memSlot = GetMemoryAddress(Mask, memSlot);

            //Get all possible memory addresses
            var memLocations = new List<string>();
            memLocations = GetAllMemAddrs(memSlot);


            //Set value to all memslots in list
            memLocations.ForEach(m => AddressList[m] = long.Parse(value));
        }

        private string GetMemoryAddress(string mask, string memSlot)
        {
            var str = "";
            for (int i = 0; i < mask.Length; i++)
            {
                str += mask[i] == '0' ? memSlot[i] : mask[i];
            }
            return str;
        }

        private List<string> GetAllMemAddrs(string mask)
        {
            var ret = new List<string>();
            var i = mask.IndexOf('X');

            //If match was found, replace with both possibilities and get new permutations
            if (i >= 0)
            {
                var m0 = mask.Remove(i, 1).Insert(i, "0");
                var m1 = mask.Remove(i, 1).Insert(i, "1");

                var l1 = GetAllMemAddrs(m0);
                var l2 = GetAllMemAddrs(m1);
                ret.AddRange(l1);
                ret.AddRange(l2);
            } else
            {
                ret.Add(mask);
            }
            return ret;
        }

        public long GetMemValuesSum()
        {
            return AddressList.Values.Sum();
        }

        private void SetMemoryValue(string instruction)
        {
            //Parse values for memory slot and value to be stored
            var memSlot = Regex.Match(instruction, @"\d+").Value;
            var value = Regex.Match(instruction, @"[0-9]+$").Value;
            var newValue = "";
            var print = false;

            //Convert provided value to binary
            value = Convert.ToString(long.Parse(value), 2).PadLeft(Mask.Length, '0');

            //Apply mask to get new binary value
            for (int i = 0; i < Mask.Length; i++)
            {
                newValue += Mask[i] == 'X' ? value[i] : Mask[i];
            }

            if (print)
            {
                Console.WriteLine($"Value:\t{value}");
                Console.WriteLine($"Mask:\t{Mask}");
                Console.WriteLine($"New:\t{newValue}");
                Console.WriteLine();
            }

            //Store value in memory as decimal
            AddressList[memSlot] = Convert.ToInt64(newValue, 2);
        }

        private string GetInstructionType(string instruction)
        {
            if (Regex.Match(instruction, @"^mask").Success)
            {
                return "mask";
            } else if (Regex.Match(instruction, @"^mem\[").Success)
            {
                return "mem";
            } else
            {
                return "unknown";
            }
        }
    }
}
