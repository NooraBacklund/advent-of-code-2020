﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14
{
    class Part1Solver
    {
        private List<string> instructions;

        public Part1Solver(List<string> instructions)
        {
            this.instructions = instructions;
        }

        public long Solve()
        {
            var mem = new Memory();
            instructions.ForEach(i => mem.ProcessInstruction(i, 1));
            return mem.GetMemValuesSum();
        }
    }
}
