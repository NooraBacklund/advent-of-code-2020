using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read in data
            var instructions = File.ReadAllLines("day14_input.txt").ToList();

            var part1 = new Part1Solver(instructions);
            Console.WriteLine($"Part 1 answer: {part1.Solve()}\n");

            var part2 = new Part2Solver(instructions);
            Console.WriteLine($"Part 2 answer: {part2.Solve()}\n");
        }
    }
}
