﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14
{
    class Part2Solver
    {
        private readonly List<string> instructions;

        public Part2Solver(List<string> instructions)
        {
            this.instructions = instructions;
        }

        internal long Solve()
        {
            var mem = new Memory();
            foreach (var instruction in instructions)
            {
                mem.ProcessInstruction(instruction, 2);
            }

            return mem.GetMemValuesSum();
        }
    }
}
