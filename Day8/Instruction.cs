﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day8
{
    class Instruction
    {
        public string Type { get; }
        public int Value { get; }
        public bool Visited = false;
        public bool Modified = false;

        public Instruction(string data)
        {
            Type = Regex.Match(data, @"[a-z]+").Value;
            Value = int.Parse(Regex.Match(data, @"-?\d+").Value);
        }
    }
}
