using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day8
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Read in data
                var data = File.ReadAllLines("day8_input.txt");

                //Generate instruction list
                List<Instruction> instructionList = data.Select(i => new Instruction(i)).ToList();

                //Part 1: Get last accumulated value before infinite loop
                TryExecuteProgram(instructionList, out var result, false);
                Console.WriteLine($"Part 1 answer: {result}");

                //Part 2: Find corrupted instruction
                Console.WriteLine($"Part 2 answer: {GetFixedProgramAccumulator(instructionList)}");
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        //Part 1
        public static bool TryExecuteProgram(List<Instruction> instructions, out int accumulator, bool attemptToCorrectInstruction)
        {
            accumulator = 0;
            var index = 0;
            var modify = attemptToCorrectInstruction;

            while (!instructions[index].Visited)
            {
                //Set as visited
                instructions[index].Visited = true;

                //Get instruction and modify if necessary (attempt to fix only a single faulty instruction)
                var instruction = instructions[index].Type;
                if (modify && (instruction != "acc") && !instructions[index].Modified)
                {
                    //Set current instruction as modified and mark the modification as completed
                    instructions[index].Modified = true;
                    modify = false;

                    //Modify instruction
                    if (instruction == "nop")
                    {
                        instruction = "jmp";
                    } else
                    {
                        instruction = "nop";
                    }
                }

                //Handle instruction
                switch (instruction)
                {
                    case "nop":
                        index++;
                        break;

                    case "acc":
                        accumulator += instructions[index].Value;
                        index++;
                        break;

                    case "jmp":
                        index += instructions[index].Value;
                        break;

                    default:
                        Console.WriteLine($"Unknown instruction: {instructions[index]}");
                        break;
                }

                //Stop execution if last instruction in list was just run (execution ended successfully)
                if (index == instructions.Count)
                {
                    return true;
                }
            }

            //If infinite loop was reached, inform that execution did not succeed
            return false;
        }

        //Part 2
        public static int GetFixedProgramAccumulator(List<Instruction> instructions)
        {
            var acc = 0;

            //Set all visited values to "false" to allow rerun of program
            instructions.Select(i => { i.Visited = false; return i; }).ToList();

            //Change instructions until corrupted instruction is found and program executes to end
            while (!TryExecuteProgram(instructions, out acc, true)) {
                instructions.Select(i => { i.Visited = false; return i; }).ToList();
            }
            return acc;
        }

    }
}
